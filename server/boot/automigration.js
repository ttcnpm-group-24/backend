'use strict';

module.exports = function(server) {
  var mysql = server.dataSources.mysql;

  console.log('-- Models found:', Object.keys(server.models));

  for (let model in server.models) {
    if (model === 'Attachment') {
      continue;
    }
    console.log(
      'Checking if table for model ' +
        model +
        ' is created and up-to-date in DB ...'
    );
    mysql.isActual(model, function(err, actual) {
      if (actual) {
        console.log('Model ' + model + ' is up-to-date.');
      } else if (err) {
        console.log(err);
      } else {
        console.log('Difference found! Auto-migrating model ' + model + ' ...');
        mysql.autoupdate(model, function() {
          console.log('Auto-migrated model ' + model + ' successfully.');
        });
      }
    });
  }
};
