FROM node:latest

ADD . /ttcnpm

VOLUME /ttcnpm

RUN mkdir /storage

WORKDIR /ttcnpm

RUN npm install

CMD export CONF_listener__port=$PORT && npm start
