'use strict';

var ATTACHMENTS_URL = '/api/Attachments/';

module.exports = function(Avatar) {
  Avatar.uploadGroup = function(ctx, options, group_id, cb) {
    if (!options) options = {};
    ctx.req.params.container = 'avatars_group';
    Avatar.app.models.Attachment.createContainer({
      name: ctx.req.params.container
    });
    Avatar.app.models.Attachment.upload(ctx.req, ctx.result, options, function(
      err,
      fileObj
    ) {
      if (err) cb(err);
      else {
        // avatar is the field name
        var avaInfo = fileObj.files.avatar[0];
        console.log(avaInfo);
        Avatar.create(
          {
            name: avaInfo.name,
            type: avaInfo.type,
            container: avaInfo.container,
            url:
              ATTACHMENTS_URL + avaInfo.container + '/download/' + avaInfo.name,
            group_id: group_id
          },
          function(err, obj) {
            if (err !== null) {
              cb(err);
            } else {
              cb(null, obj);
            }
          }
        );
      }
    });
  };

  Avatar.remoteMethod('uploadGroup', {
    description: 'Upload an avatar for a group',
    accepts: [
      { arg: 'ctx', type: 'object', http: { source: 'context' } },
      { arg: 'options', type: 'object', http: { source: 'query' } },
      {
        arg: 'group_id',
        type: 'number',
        http: { source: 'query' },
        required: true
      }
    ],
    returns: {
      arg: 'fileObject',
      type: 'object',
      root: true
    },
    http: { verd: 'post' }
  });

  Avatar.uploadClient = function(ctx, options, client_id, cb) {
    if (!options) options = {};
    ctx.req.params.container = 'avatars_client';
    Avatar.app.models.Attachment.createContainer({
      name: ctx.req.params.container
    });
    Avatar.app.models.Attachment.upload(ctx.req, ctx.result, options, function(
      err,
      fileObj
    ) {
      if (err) cb(err);
      else {
        // avatar is the field name
        var avaInfo = fileObj.files.avatar[0];
        console.log(avaInfo);
        Avatar.create(
          {
            name: avaInfo.name,
            type: avaInfo.type,
            container: avaInfo.container,
            url:
              ATTACHMENTS_URL + avaInfo.container + '/download/' + avaInfo.name,
            client_id: client_id
          },
          function(err, obj) {
            if (err !== null) {
              cb(err);
            } else {
              cb(null, obj);
            }
          }
        );
      }
    });
  };

  Avatar.remoteMethod('uploadClient', {
    description: 'Upload an avatar for a client',
    accepts: [
      { arg: 'ctx', type: 'object', http: { source: 'context' } },
      { arg: 'options', type: 'object', http: { source: 'query' } },
      {
        arg: 'client_id',
        type: 'number',
        http: { source: 'query' },
        required: true
      }
    ],
    returns: {
      arg: 'fileObject',
      type: 'object',
      root: true
    },
    http: { verd: 'post' }
  });
};
